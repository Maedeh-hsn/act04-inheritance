public class Book{
    protected String title;
    private String author;
    
    
    public Book(String title, String author){
        this.title = title;
        this.author = author;
    }

    public String getTitle(){
        return this.title;
    }

    public String getAuthor(){
        return this.author;
    }

    public String ToString(){
        return "The title is:"  + this.title + "\n The author is " + this.author;
    }
}