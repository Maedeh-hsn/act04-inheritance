public class ElectronicBook  extends Book{
    private double numberBytes;

    public ElectronicBook(String title, String author, double numberBytes){
        super(title, author);
        this.numberBytes = numberBytes;
    }

    public double getNumberBytes(){
        return this.numberBytes;
    } 

    public String ToString(){
        String fromBase = super.ToString();
        fromBase = "\n The Number of bytes is: " + this.numberBytes;
        return fromBase;
    }
    
}
